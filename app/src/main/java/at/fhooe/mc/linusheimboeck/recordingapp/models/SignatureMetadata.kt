package at.fhooe.mc.linusheimboeck.recordingapp.models

import java.lang.StringBuilder

class SignatureMetadata(
    var xdpi: Number,
    var ydpi: Number,
    var screenWidth: Number,
    var screenHeight: Number,
    var position: String?,
    var sex: String?,
    var age: String?,
    var country: String?,
    var language: String?,
    var writingHand: String?
) {

    fun statisticalDataString(): String {
        val builder = StringBuilder().append(position).append("-")
            .append(sex).append("-")
            .append(age).append("-")
            .append(country).append("-")
            .append(language).append("-")
            .append(writingHand)
        return builder.toString()
    }
}