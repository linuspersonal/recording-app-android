package at.fhooe.mc.linusheimboeck.recordingapp.models

import java.io.File

class Signature(
    var firstName: String,
    var lastName: String,
    var timestamp: Long,
    var genuine: Boolean,
    var signatureFile: File
) {
}