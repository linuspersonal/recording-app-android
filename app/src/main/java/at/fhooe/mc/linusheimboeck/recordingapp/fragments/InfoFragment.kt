package at.fhooe.mc.linusheimboeck.recordingapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import at.fhooe.mc.linusheimboeck.recordingapp.R

class InfoFragment : Fragment() {

    companion object {
        fun newInstance() =
            InfoFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

}
