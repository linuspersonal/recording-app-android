package at.fhooe.mc.linusheimboeck.recordingapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.fragments.SignatureListFragment
import kotlinx.android.synthetic.main.activity_signature_overview.*

class SignatureOverviewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature_overview)

        val viewingOnly = intent.getBooleanExtra("viewingOnly", true)
        val genuineOnly = intent.getBooleanExtra("genuineOnly", false)

        if (!viewingOnly) {
            overviewActivityToolBar.title = getString(R.string.overviewActivityToolBarSelectTitle)
        }

        val fragment: SignatureListFragment = supportFragmentManager.findFragmentById(R.id.activitySigOverviewListFragment) as SignatureListFragment
        fragment.setListProperties(viewingOnly, genuineOnly)
    }
}
