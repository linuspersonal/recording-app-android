package at.fhooe.mc.linusheimboeck.recordingapp.interfaces

import android.view.View

interface RecyclerViewItemClickedListener {

    fun onRecyclerViewItemClicked(pos: Int, v: View?)
}