package at.fhooe.mc.linusheimboeck.recordingapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.fragments.DisclaimerFragment
import at.fhooe.mc.linusheimboeck.recordingapp.fragments.InfoFragment

class InformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)

        val content = intent.getStringExtra("content")

        val fragment = if (content == "disclaimer") {
            DisclaimerFragment()
        } else {
            InfoFragment()
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.infoFragmentContainer, fragment)
        transaction.commit()

    }
}
