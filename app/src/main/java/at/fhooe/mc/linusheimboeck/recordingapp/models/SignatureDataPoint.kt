package at.fhooe.mc.linusheimboeck.recordingapp.models

import java.io.Serializable

class SignatureDataPoint(

    // Screen
    var x: Float?,
    var y: Float?,

    // Pixels/second
    var vX: Float?,
    var vY: Float?,

    var strokeNumber: Int?,
    var touchSize: Float?,

    // Gyro in rad/s
    var gyroX: Float,
    var gyroY: Float,
    var gyroZ: Float,

    // Magnetometer in microTesla
    var magX: Float,
    var magY: Float,
    var magZ: Float,

    // Linear Accelerometer in m/s^2
    var linAccX: Float,
    var linAccY: Float,
    var linAccZ: Float,

    // Gravity sensor in m/s^2
    var gravX: Float,
    var gravY: Float,
    var gravZ: Float,

    /**
     * In milliseconds
     */
    var timestamp: Long
) {

    fun getSignaturePoint(): SignaturePoint? {
        if (x == null || y == null || vX == null || vY == null || strokeNumber == null || touchSize == null) {
            return null
        }
        return SignaturePoint(x, y, vX, vY, strokeNumber, touchSize, timestamp)
    }

    override fun toString(): String {
        return StringBuilder()
            .append(x).append(",")
            .append(y).append(",")
            .append(vX).append(",")
            .append(vY).append(",")
            .append(strokeNumber).append(",")
            .append(touchSize).append(",")
            .append(gyroX).append(",")
            .append(gyroY).append(",")
            .append(gyroZ).append(",")
            .append(magX).append(",")
            .append(magY).append(",")
            .append(magZ).append(",")
            .append(linAccX).append(",")
            .append(linAccY).append(",")
            .append(linAccZ).append(",")
            .append(gravX).append(",")
            .append(gravY).append(",")
            .append(gravZ).append(",")
            .append(timestamp).toString()
    }
}