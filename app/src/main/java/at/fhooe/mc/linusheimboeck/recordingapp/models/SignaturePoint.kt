package at.fhooe.mc.linusheimboeck.recordingapp.models

class SignaturePoint(
    var x: Float?,
    var y: Float?,
    var vX: Float?,
    var vY: Float?,
    var strokeNumber: Int?,
    var touchSize: Float?,

    /**
     * In SystemClock.uptimeMillis()
     */
    var eventTime: Long
) {

    override fun toString(): String {
        return StringBuilder()
            .append(x).append(",")
            .append(y).append(",")
            .append(vX).append(",")
            .append(vY).append(",")
            .append(strokeNumber).append(",")
            .append(touchSize).append(",")
            .append(eventTime).toString()
    }

}