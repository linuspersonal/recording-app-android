package at.fhooe.mc.linusheimboeck.recordingapp.fragments

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureDataPoint
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureMetadata
import at.fhooe.mc.linusheimboeck.recordingapp.viewmodels.SigningPadViewModel
import at.fhooe.mc.linusheimboeck.recordingapp.views.SignatureView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.signing_pad_fragment.*
import kotlinx.android.synthetic.main.signing_pad_fragment.view.*
import kotlin.math.roundToInt


class SigningPadFragment : Fragment(), View.OnClickListener, View.OnTouchListener {

    val REQUEST_EXTERNAL_STORAGE_PERMISSION = 1;

    companion object {
        fun newInstance() =
            SigningPadFragment()
    }

    private var permanentSignature: ArrayList<SignatureDataPoint>? = null
    private var viewingOnly: Boolean = false

    private var signatureDrawingStarted = false
    private var signatureSaved = false

    private var signatureCounter = 0

    private lateinit var viewModel: SigningPadViewModel
    private lateinit var signatureView: SignatureView

    private val TAG = "SigningPadFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.signing_pad_fragment, container, false)

        view.buttonSigningPadClose.setOnClickListener(this)
        view.buttonSigningPadUndo.setOnClickListener(this)
        view.buttonSigningPadSave.setOnClickListener(this)
        view.buttonSigningPadGrantPermission.setOnClickListener(this)
        view.buttonSigningPadNext.setOnClickListener(this)

        view.signingPadCounterTextView.text = requireContext().getString(R.string.signingPadCounterText, signatureCounter.toString(), "X")

        signatureView = view.signatureView
        signatureView.setOnTouchListener(this)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SigningPadViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()

        if (!hasStoragePermissions()) {

            if (isEditableMode()) {
                // When in viewing mode only, we don't need storage permission, so dont ask in that case
                requestStoragePermissions()
            }
        }
    }

    override fun onPause() {
        super.onPause()

        clearSignature()
        signatureView.stopMotionSensors()
    }

    override fun onResume() {
        super.onResume()

        updateUi()
        if (isEditableMode()) {
            signatureView.startMotionSensors()
        }
    }

    override fun onClick(view: View?) {

        when (view?.id) {
            R.id.buttonSigningPadGrantPermission -> {
                if (!hasStoragePermissions()) {

                    requestStoragePermissions()
                }
            }
            R.id.buttonSigningPadNext -> {
                clearSignature()
                signatureView.clearSignatureBackground()
                signatureView.setSigningEnabled(true)
                signatureSaved = false
                updateUi()
            }
            R.id.buttonSigningPadClose -> {

                if (isEditableMode()) {
                    val dialogBuilder = MaterialAlertDialogBuilder(requireActivity())
                    dialogBuilder.setTitle(requireContext().getString(R.string.signingPadExitDialogTitle))
                    dialogBuilder.setCancelable(false)
                    dialogBuilder.setMessage(requireContext().getString(R.string.signingPadExitDialogMessage, signatureCounter.toString()))
                    dialogBuilder.setPositiveButton(requireContext().getString(R.string.signingPadExitDialogButtonPositive)) { _, _ ->
                        activity?.finish()
                    }
                    dialogBuilder.setNegativeButton(requireContext().getString(R.string.signingPadExitDialogButtonNegative)) { dialog, _ -> dialog.cancel()}
                    dialogBuilder.show()
                } else {
                    activity?.finish()
                }

            }
            R.id.buttonSigningPadUndo -> {

                clearSignature()
                updateUi()
            }
            R.id.buttonSigningPadSave -> {
                val rawSignature = signatureView.getRawSignature()
                rawSignature.genuine = permanentSignature == null

                if (rawSignature.signaturePoints.size > 0 && isSaveEnabled()) {
                    signatureSaved = true
                    signatureView.setSigningEnabled(false)
                    signatureView.setSignatureBackground(rawSignature.signaturePoints)
                    clearSignature()
                    updateUi()

                    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
                    val firstName = prefs.getString("firstName", "Unknown")?.replace("-", "+")   // To deal with first names like Anna-Maria
                    val lastName = prefs.getString("lastName", "Unknown")?.replace("-", "+")     // To deal with last names like Pfeifer-Feldhofer

                    val position = prefs.getString("position", "unknown")
                    val sex = prefs.getString("gender", "unknown")
                    val age = prefs.getString("age", "unknown")
                    val country = prefs.getString("country", "unknown")
                    val nativeLanguage = prefs.getString("nativeLanguage", "unknown")
                    val writingHand = prefs.getString("writingHand", "unknown")

                    val xdpi = requireContext().resources.displayMetrics.xdpi.roundToInt()      // Int precision is enough
                    val ydpi = requireContext().resources.displayMetrics.ydpi.roundToInt()      // Int precision is enough
                    val screenWidth = requireContext().resources.displayMetrics.widthPixels
                    val screenHeight = requireContext().resources.displayMetrics.heightPixels

                    val metadata = SignatureMetadata(xdpi, ydpi, screenWidth, screenHeight, position, sex, age, country, nativeLanguage, writingHand)

                    val savedSignature = viewModel.saveSignature(rawSignature, firstName, lastName, metadata)

                    // TODO: Move to callback when saveSignature is async
                    if (savedSignature != null) {
                        // Saving worked
                        signatureCounter++
                        signingPadCounterTextView.text = requireContext().getString(R.string.signingPadCounterText, signatureCounter.toString(), "X")
                        signatureView.setSignatureBackgroundDataPoint(savedSignature)
                    } else {
                        // Saving failed
                        signatureSaved = false
                        signatureView.setSigningEnabled(true)
                        signatureView.clearSignatureBackground()
                        Toast.makeText(requireContext(), R.string.toastSaveSignatureFailed, Toast.LENGTH_SHORT).show()
                        clearSignature()
                        updateUi()
                    }
                } else {
                    Toast.makeText(requireContext(), R.string.toastSaveSignatureFailed, Toast.LENGTH_SHORT).show()
                    clearSignature()
                    updateUi()
                    return
                }
            }
        }
    }

    private fun hasStoragePermissions(): Boolean {

        return ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestStoragePermissions() {

        requestPermissions(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
            REQUEST_EXTERNAL_STORAGE_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            REQUEST_EXTERNAL_STORAGE_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    updateUi()
                    buttonSigningPadGrantPermission.visibility = View.GONE
                } else {
                    buttonSigningPadGrantPermission.visibility = View.VISIBLE
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {

        if (!signatureView.isPristine() && !signatureDrawingStarted) {
            signatureDrawingStarted = true
            updateUi()
        }

        return false
    }

    fun setPermanentSignatureBackground(signature: ArrayList<SignatureDataPoint>, viewingOnly: Boolean) {

        if (viewingOnly) {
            signatureView.setSigningEnabled(false)
            signatureView.setPermanentBackgroundDataPoint(signature, Color.BLACK)
            signatureView.stopMotionSensors()
        } else {
            signatureView.setPermanentBackgroundDataPoint(signature, ContextCompat.getColor(requireContext(), R.color.signingPadBackgroundText))
        }
        this.permanentSignature = signature
        this.viewingOnly = viewingOnly
        updateUi()
    }

    private fun updateUi() {

        buttonSigningPadSave.isEnabled = isSaveEnabled()
        buttonSigningPadUndo.isEnabled = isUndoEnabled()

        if (isEditableMode()) {
            buttonSigningPadSave.visibility = View.VISIBLE
            buttonSigningPadUndo.visibility = View.VISIBLE
            signingPadCounterTextView.visibility = View.VISIBLE
        } else {
            buttonSigningPadSave.visibility = View.GONE
            buttonSigningPadUndo.visibility = View.GONE
            signingPadCounterTextView.visibility = View.GONE
        }

        if(isSignHereTextVisible()) {
            signingPadBackgroundTextView.visibility = View.VISIBLE
        } else {
            signingPadBackgroundTextView.visibility = View.GONE
        }

        if(isNewSignatureVisible()) {
            buttonSigningPadNext.visibility = View.VISIBLE
        } else {
            buttonSigningPadNext.visibility = View.GONE
        }
    }

    private fun isSignHereTextVisible() : Boolean {

        return !signatureDrawingStarted && !signatureSaved && permanentSignature == null
    }

    private fun isNewSignatureVisible() : Boolean {

        return signatureSaved && hasStoragePermissions() && !signatureDrawingStarted && isEditableMode()
    }

    private fun isUndoEnabled() : Boolean {

        return signatureDrawingStarted && !signatureSaved && isEditableMode()
    }

    private fun isSaveEnabled() : Boolean {

        return hasStoragePermissions() && signatureDrawingStarted && !signatureSaved && isEditableMode()
    }

    private fun isEditableMode() : Boolean {
        return (permanentSignature == null || permanentSignature != null && !viewingOnly)
    }

    private fun clearSignature() {
        signatureView.clearSignature()
        signatureDrawingStarted = false
    }

}
