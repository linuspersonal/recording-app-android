package at.fhooe.mc.linusheimboeck.recordingapp.utils

import android.os.Build
import android.os.Environment
import android.util.Log
import at.fhooe.mc.linusheimboeck.recordingapp.models.Signature
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureDataPoint
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureMetadata
import java.io.File
import java.io.FilenameFilter

class FileUtils {

    private val TAG = "FileWriter"

    val VALUE_AMOUNT = 19
    val VALUE_ORDER_STRING = "x,y,vX,vY,strokenumber,touchsize,gyroX,gyroY,gyroZ,magX,magY,magZ,accX,accY,accZ,gravX,gravY,gravZ,timestamp"

    fun writeSignature(signaturePoints: ArrayList<SignatureDataPoint>, signature: Signature, metadata: SignatureMetadata) {

        writeSignatureString(getSignatureString(signaturePoints), signature, metadata)
    }

    fun readSignature(signature: File): ArrayList<SignatureDataPoint>? {

        val signatureString = readSignatureString(signature) ?: return null

        return signatureStringToSignatureDataPointList(signatureString)
    }

    fun signatureStringToSignatureDataPointList(signatureString: String): ArrayList<SignatureDataPoint>? {

        val linesArray = signatureString.split("\n")

        // Ignore header line
        val result = ArrayList<SignatureDataPoint>()
        for (i in 1 until linesArray.size) {

            val current = getDataPointFromString(linesArray[i])
            if (current != null) {
                result.add(current)
            } else {
                // Corrupt file, return null
                return null
            }
        }

        return result
    }

    private fun writeSignatureString(signature: String, signatureData: Signature, metadata: SignatureMetadata) {

        val folder = File(Environment.getExternalStorageDirectory(), signatureData.signatureFile.path)

        if (!folder.exists()) {
            folder.mkdir()
        }

        var count = 0
        var file = File(folder,"sig$count-${signatureData.firstName}-${signatureData.lastName}-${signatureData.timestamp}-${signatureData.genuine}-" +
                "${metadata.screenWidth}-${metadata.screenHeight}-${metadata.xdpi}-${metadata.ydpi}-${Build.MODEL.replace("-", "")}-" +
                "${metadata.statisticalDataString()}.csv")

        while (file.exists()) {
            count++
            file = File(folder,"sig$count-${signatureData.firstName}-${signatureData.lastName}-${signatureData.timestamp}-${signatureData.genuine}-" +
                    "${metadata.screenWidth}-${metadata.screenHeight}-${metadata.xdpi}-${metadata.ydpi}-${Build.MODEL.replace("-", "")}-" +
                    "${metadata.statisticalDataString()}.csv")
        }

        file.appendText(signature)

    }

    fun readSignatureString(signature: File): String? {

        if (!signature.exists() || !signature.isFile || signature.extension != "csv") {
            return null
        }

        return signature.readText()
    }

    private fun getDataPointFromString(string: String): SignatureDataPoint? {
        val splits = string.split(",")

        if (splits.size != VALUE_AMOUNT) {
            return null
        }

        return try {
            SignatureDataPoint(
                splits[0].toFloatOrNull(), splits[1].toFloatOrNull(), splits[2].toFloatOrNull(), splits[3].toFloatOrNull(), splits[4].toIntOrNull(), splits[5].toFloatOrNull(),     // Signature
                splits[6].toFloat(), splits[7].toFloat(), splits[8].toFloat(),  // Gyro
                splits[9].toFloat(), splits[10].toFloat(), splits[11].toFloat(),  // Magnetometer
                splits[12].toFloat(), splits[13].toFloat(), splits[14].toFloat(),  // Accelerometer
                splits[15].toFloat(), splits[16].toFloat(), splits[17].toFloat(), // Gravity
                splits[18].toLong())    // Timestamp

        } catch (e: NumberFormatException) {
            null
        }
    }

    fun getSignatureString(signaturePoints: ArrayList<SignatureDataPoint>): String {

        val builder = StringBuilder()
        builder.append(VALUE_ORDER_STRING)
        signaturePoints.forEach {
            builder.append("\n").append(it.toString())
        }

        return builder.toString()
    }

    fun getSignatureList(relativePath: String): ArrayList<Signature> {

        val signatureFiles = listSignatureFiles(relativePath)

        val result: ArrayList<Signature> = ArrayList()
        signatureFiles?.forEach {
            val nameParams = it.nameWithoutExtension.split("-")
            if (nameParams.size == 5 || nameParams.size == 10 || nameParams.size == 16) {                 // Legacy name format has 5, current name format has 10 params
                // Ignore prefix
                result.add(Signature(nameParams[1], nameParams[2], nameParams[3].toLong(), nameParams[4].toBoolean(), it))
            }
        }

        return result
    }

    private fun listSignatureFiles(relativePath: String): Array<File>? {

        val folder = File(Environment.getExternalStorageDirectory(), relativePath)

        return folder.listFiles { _, filename ->
            filename.endsWith(".csv") && filename.startsWith("sig")
        }
    }

    fun zipSignatures(): String {

        val zipPath = Environment.getExternalStorageDirectory().path + "/Signatures/signatures.zip"
        val signatureList = listSignatureFiles("/Signatures")
        ZipManager.zip(signatureList, zipPath)
        return zipPath
    }

}