package at.fhooe.mc.linusheimboeck.recordingapp.fragments

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceManager
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.activities.InformationActivity
import at.fhooe.mc.linusheimboeck.recordingapp.activities.SignatureOverviewActivity
import at.fhooe.mc.linusheimboeck.recordingapp.activities.SigningPadActivity
import at.fhooe.mc.linusheimboeck.recordingapp.utils.FileUtils
import at.fhooe.mc.linusheimboeck.recordingapp.viewmodels.MainMenuViewModel
import kotlinx.android.synthetic.main.main_menu_fragment.*
import kotlinx.android.synthetic.main.main_menu_fragment.view.*
import java.io.File


class MainMenuFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance() =
            MainMenuFragment()
    }

    private val TAG = "MainMenuFragment"
    private lateinit var viewModel: MainMenuViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.main_menu_fragment, container, false)

        view.cardActionButtonSignatureRecording.setOnClickListener(this)
        view.cardActionButtonSignatureForging.setOnClickListener(this)
        view.cardActionButtonSignatureOverview.setOnClickListener(this)
        view.cardActionButtonSignatureDisclaimer.setOnClickListener(this)
        view.cardActionButtonDualScreen.setOnClickListener(this)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainMenuViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()

        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        var firstName = prefs.getString("firstName", null)

        if (firstName == null) {
            firstName = "Stranger"
        }

        title_heading.text = getString(R.string.title_heading_text_personal, firstName)
    }

    override fun onClick(view: View?) {

        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())

        when(view?.id) {
            R.id.cardActionButtonSignatureRecording -> {

                if (!prefs.getBoolean("disclaimerAgreed", false)) {
                    Toast.makeText(requireContext(), "Please agree to disclaimer in settings", Toast.LENGTH_SHORT).show()
                } else {
                    val intent = Intent(activity, SigningPadActivity::class.java)
                    startActivity(intent)
                }
            }
            R.id.cardActionButtonSignatureForging -> {

                if (!prefs.getBoolean("disclaimerAgreed", false)) {
                    Toast.makeText(requireContext(), "Please agree to disclaimer in settings", Toast.LENGTH_SHORT).show()
                } else {
                    val intent = Intent(activity, SignatureOverviewActivity::class.java)
                    val bundle = Bundle()
                    bundle.putBoolean("viewingOnly", false)
                    bundle.putBoolean("genuineOnly", true)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
            }
            R.id.cardActionButtonSignatureOverview -> {
                val intent = Intent(activity, SignatureOverviewActivity::class.java)
                val bundle = Bundle()
                bundle.putBoolean("viewingOnly", true)
                bundle.putBoolean("genuineOnly", false)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            R.id.cardActionButtonSignatureDisclaimer -> {
                // Toast.makeText(context, "Disclaimer clicked", Toast.LENGTH_SHORT).show()
                val intent = Intent(activity, InformationActivity::class.java)
                val bundle = Bundle()
                bundle.putString("content", "disclaimer")
                intent.putExtras(bundle)
                startActivity(intent)
            }
            R.id.cardActionButtonDualScreen -> {
                Toast.makeText(context, "Dual Screen clicked", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
