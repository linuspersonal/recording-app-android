package at.fhooe.mc.linusheimboeck.recordingapp.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.interfaces.RecyclerViewItemClickedListener
import at.fhooe.mc.linusheimboeck.recordingapp.models.Signature
import kotlinx.android.synthetic.main.signature_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SignatureRecyclerAdapter(var signatures: ArrayList<Signature>, var callback: RecyclerViewItemClickedListener) : RecyclerView.Adapter<SignatureRecyclerAdapter.SignatureHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SignatureRecyclerAdapter.SignatureHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.signature_list_item, parent, false)
        return SignatureHolder(inflatedView, callback)
    }

    override fun getItemCount(): Int {
        return signatures.size
    }

    override fun onBindViewHolder(holder: SignatureRecyclerAdapter.SignatureHolder, position: Int) {
        val signatureItem = signatures[position]
        holder.bindSignature(signatureItem)
    }

    class SignatureHolder(v: View, callback: RecyclerViewItemClickedListener) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private val TAG = "SignatureHolder"

        private var view: View = v
        private var onClickCallback = callback
        private var signature: Signature? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onClickCallback.onRecyclerViewItemClicked(adapterPosition, v)
        }

        companion object {

            private val SIGNATURE_KEY = "SIGNATURE"
        }

        fun bindSignature(signature: Signature) {
            this.signature = signature

            if (!signature.genuine) {
                view.signatureListItemImageView.setImageResource(R.drawable.ic_people_alt_24px)
                view.signatureListItemForgeryTextView.setText(R.string.signatureListTypeTextForgery)
            } else {
                view.signatureListItemImageView.setImageResource(R.drawable.ic_edit_24px)
                view.signatureListItemForgeryTextView.setText(R.string.signatureListTypeTextGenuine)
            }

            view.signatureListItemNameTextView.text = view.context.getString(R.string.signatureListNameText, signature.firstName, signature.lastName)

            val dateFormat = SimpleDateFormat("dd-MM-yyyy")
            view.signatureListItemDateTextView.text = dateFormat.format(Date(signature.timestamp))

        }

    }
}