package at.fhooe.mc.linusheimboeck.recordingapp.fragments

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager

import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.activities.SigningPadActivity
import at.fhooe.mc.linusheimboeck.recordingapp.adapters.SignatureRecyclerAdapter
import at.fhooe.mc.linusheimboeck.recordingapp.interfaces.RecyclerViewItemClickedListener
import at.fhooe.mc.linusheimboeck.recordingapp.models.Signature
import at.fhooe.mc.linusheimboeck.recordingapp.viewmodels.SignatureListViewModel
import kotlinx.android.synthetic.main.signature_list_fragment.*
import kotlinx.android.synthetic.main.signature_list_fragment.view.*

class SignatureListFragment : Fragment(), RecyclerViewItemClickedListener {

    private val TAG = "SignatureListFragment"

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: SignatureRecyclerAdapter

    private var viewingOnly = true
    private var genuineOnly = false

    private var signatureList = ArrayList<Signature>()

    companion object {
        fun newInstance() = SignatureListFragment()
    }

    private lateinit var viewModel: SignatureListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.signature_list_fragment, container, false)

        linearLayoutManager = LinearLayoutManager(requireContext())
        view.signatureOverviewFragmentRecyclerView.layoutManager = linearLayoutManager

        adapter = SignatureRecyclerAdapter(signatureList, this)
        view.signatureOverviewFragmentRecyclerView.adapter = adapter

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SignatureListViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()

        refreshSignatureList()
    }

    override fun onRecyclerViewItemClicked(pos: Int, v: View?) {
        // Load data as signature string to pass it to activity
        val signatureData = viewModel.getSignatureData(signatureList[pos])

        if (signatureData == null) {
            Toast.makeText(requireContext(), R.string.signatureListFileNotFound, Toast.LENGTH_SHORT).show()
            refreshSignatureList()
        } else {
            val intent = Intent(requireActivity(), SigningPadActivity::class.java)
            val bundle = Bundle()
            bundle.putString("signature", signatureData)
            bundle.putBoolean("viewingOnly", viewingOnly)
            intent.putExtras(bundle)
            startActivity(intent)

            if (!viewingOnly) {
                // Close current activity
                requireActivity().finish()
            }
        }
    }

    fun refreshSignatureList() {
        signatureList = viewModel.getSignatureList(genuineOnly)
        adapter.signatures = signatureList
        adapter.notifyDataSetChanged()

        if (signatureList.size > 0) {
            signatureListFragmentEmptyTextView.visibility = View.GONE
        } else {
            signatureListFragmentEmptyTextView.visibility = View.VISIBLE
        }
    }

    fun setListProperties(viewingOnly: Boolean, genuineOnly: Boolean) {
        this.viewingOnly = viewingOnly
        this.genuineOnly = genuineOnly
    }
}
