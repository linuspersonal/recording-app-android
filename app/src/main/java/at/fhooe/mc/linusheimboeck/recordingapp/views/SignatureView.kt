package at.fhooe.mc.linusheimboeck.recordingapp.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.SystemClock
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import androidx.collection.CircularArray
import androidx.core.content.ContextCompat
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.models.RawSignature
import at.fhooe.mc.linusheimboeck.recordingapp.models.SensorMeasurement
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureDataPoint
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignaturePoint
import kotlin.collections.ArrayList

class SignatureView(context: Context, attrs: AttributeSet) : View(context, attrs), SensorEventListener {

    val TAG = "SignatureView"
    val BUFFER_CAPACITY = 20

    private val signaturePath = Path()
    private val backgroundPath = Path()
    private val permanentPath = Path()

    private val paint = Paint()
    private val backgroundPaint = Paint()
    private val permanentPaint = Paint()

    private var velocityTracker: VelocityTracker? = null

    private var sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private var gyroscopeSensor: Sensor
    private var linearAccelerationSensor: Sensor
    private var magneticFieldSensor: Sensor
    private var gravitySensor: Sensor

    private var gyroscopeBuffer: CircularArray<SensorMeasurement> = CircularArray()
    private var magneticFieldBuffer: CircularArray<SensorMeasurement> = CircularArray()
    private var linearAccelerationBuffer: CircularArray<SensorMeasurement> = CircularArray()
    private var gravityBuffer: CircularArray<SensorMeasurement> = CircularArray()

    private var pristine = true
    private var motionSensorsEnabled = false
    private var signingEnabled = true

    private var points = ArrayList<SignaturePoint>()
    private var strokeNumber = 0

    init {
        context.theme.obtainStyledAttributes(attrs,
            R.styleable.SignatureView, 0, 0).apply {

            try {
                paint.color = getColor(R.styleable.SignatureView_signatureStrokeColor, Color.BLACK)
                backgroundPaint.color = getColor(
                    R.styleable.SignatureView_backgroundSignatureStrokeColor,
                    ContextCompat.getColor(context,
                        R.color.backgroundSignatureStrokeColor
                    ))
            } finally {
                recycle()
            }
        }

        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = 10f

        backgroundPaint.style = Paint.Style.STROKE
        backgroundPaint.strokeJoin = Paint.Join.ROUND
        backgroundPaint.strokeCap = Paint.Cap.ROUND
        backgroundPaint.strokeWidth = 10f

        permanentPaint.style = Paint.Style.STROKE
        permanentPaint.strokeJoin = Paint.Join.ROUND
        permanentPaint.strokeCap = Paint.Cap.ROUND
        permanentPaint.strokeWidth = 10f

        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        linearAccelerationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
        magneticFieldSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY)
    }

    override fun onDraw(canvas: Canvas?) {

        canvas?.drawPath(permanentPath, permanentPaint)

        canvas?.drawPath(backgroundPath, backgroundPaint)

        canvas?.drawPath(signaturePath, paint)
        super.onDraw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        if (!signingEnabled) {
            return true
        }
        when (event?.action) {

            MotionEvent.ACTION_DOWN -> {

                pristine = false
                velocityTracker?.clear()
                velocityTracker = velocityTracker ?: VelocityTracker.obtain()
                velocityTracker?.addMovement(event)
                signaturePath.moveTo(event.x, event.y)
            }
            MotionEvent.ACTION_MOVE -> {

                velocityTracker?.addMovement(event)
                velocityTracker?.computeCurrentVelocity(1000)

                points.add(
                    SignaturePoint(
                        event.x,
                        event.y,
                        velocityTracker?.xVelocity,
                        velocityTracker?.yVelocity,
                        strokeNumber,
                        event.size,
                        event.eventTime
                    )
                )

                signaturePath.lineTo(event.x, event.y)
                invalidate()
            }
            MotionEvent.ACTION_UP -> {

                strokeNumber++
                velocityTracker?.recycle()
                velocityTracker = null
            }
        }
        return true
    }

    fun clearSignature() {

        pristine = true
        clearBuffers()
        signaturePath.reset()
        points = ArrayList()
        strokeNumber = 0
        invalidate()
    }

    fun getRawSignature(): RawSignature {

        // Difference caused by order of obtaining time is negligible
        val utm = SystemClock.uptimeMillis()
        val ert = SystemClock.elapsedRealtime()

        return RawSignature(
            points,
            linearAccelerationBuffer,
            magneticFieldBuffer,
            gyroscopeBuffer,
            gravityBuffer,
            ert - utm,
            true
        )
    }

    fun isPristine() : Boolean {

        return pristine
    }

    fun setSigningEnabled(enabled: Boolean) {
        signingEnabled = enabled
    }

    fun setSignatureBackground(signaturePoints: ArrayList<SignaturePoint>) {

        backgroundPath.reset()
        fillPathWithSignaturePoints(signaturePoints, backgroundPath)
        invalidate()
    }

    fun setSignatureBackgroundDataPoint(signatureDataPoints: ArrayList<SignatureDataPoint>) {

        val points = toSignaturePointList(signatureDataPoints)
        setSignatureBackground(points)
    }

    fun setPermanentBackgroundDataPoint(signatureDataPoints: ArrayList<SignatureDataPoint>, color: Int) {

        val points = toSignaturePointList(signatureDataPoints)
        permanentPath.reset()
        permanentPaint.color = color
        fillPathWithSignaturePoints(points, permanentPath)
        invalidate()
    }

    private fun toSignaturePointList(signatureDataPoints: ArrayList<SignatureDataPoint>): ArrayList<SignaturePoint> {

        val points = ArrayList<SignaturePoint>()
        signatureDataPoints.forEach {
            val current = it.getSignaturePoint()
            if (current != null) {
                points.add(current)
            }
        }
        return points
    }

    fun clearSignatureBackground() {
        backgroundPath.reset()
        invalidate()
    }

    private fun fillPathWithSignaturePoints(signaturePoints: ArrayList<SignaturePoint>, path: Path) {

        if (signaturePoints.size < 1) {
            return
        }

        var currentStroke = -1
        signaturePoints.forEach {
            val xPos = it.x
            val yPos = it.y
            val strokeNr = it.strokeNumber

            if (xPos != null && yPos != null && strokeNr != null) {
                // Only use the values for drawing that belong to a stroke
                when {
                    strokeNr > currentStroke -> {

                        path.moveTo(xPos, yPos)
                        currentStroke = strokeNr
                    }
                    it.strokeNumber == currentStroke -> {
                        path.lineTo(xPos, yPos)
                    }
                    else -> {
                        // Erroneous file, return
                        path.reset()
                        return
                    }
                }
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, sensorId: Int) {
        // Not needed for now
    }

    override fun onSensorChanged(event: SensorEvent?) {

        when(event?.sensor?.type) {
            Sensor.TYPE_LINEAR_ACCELERATION -> {
                // Log.i(TAG, "V0: " + event.values[0] / 1000000L)
                if (linearAccelerationBuffer.size() >= BUFFER_CAPACITY && pristine) {
                    linearAccelerationBuffer.removeFromStart(linearAccelerationBuffer.size() - BUFFER_CAPACITY + 1)
                }
                linearAccelerationBuffer.addLast(
                    SensorMeasurement(
                        event.timestamp,
                        event.values.toList()
                    )
                )
            }
            Sensor.TYPE_GYROSCOPE -> {
                if (gyroscopeBuffer.size() >= BUFFER_CAPACITY && pristine) {
                    gyroscopeBuffer.removeFromStart(gyroscopeBuffer.size() - BUFFER_CAPACITY + 1)
                }
                gyroscopeBuffer.addLast(
                    SensorMeasurement(
                        event.timestamp,
                        event.values.toList()
                    )
                )
            }
            Sensor.TYPE_MAGNETIC_FIELD -> {
                if (magneticFieldBuffer.size() >= BUFFER_CAPACITY && pristine) {
                    magneticFieldBuffer.removeFromStart(magneticFieldBuffer.size() - BUFFER_CAPACITY + 1)
                }
                magneticFieldBuffer.addLast(
                    SensorMeasurement(
                        event.timestamp,
                        event.values.toList()
                    )
                )
            }
            Sensor.TYPE_GRAVITY -> {
                if (gravityBuffer.size() >= BUFFER_CAPACITY && pristine) {
                    gravityBuffer.removeFromStart(gravityBuffer.size() - BUFFER_CAPACITY + 1)
                }
                gravityBuffer.addLast(
                    SensorMeasurement(
                        event.timestamp,
                        event.values.toList()
                    )
                )
            }
        }
    }

    fun startMotionSensors() {
        motionSensorsEnabled = true
        sensorManager.registerListener(this, linearAccelerationSensor, SensorManager.SENSOR_DELAY_GAME)
        sensorManager.registerListener(this, gyroscopeSensor, SensorManager.SENSOR_DELAY_GAME)
        sensorManager.registerListener(this, magneticFieldSensor, SensorManager.SENSOR_DELAY_GAME)
        sensorManager.registerListener(this, gravitySensor, SensorManager.SENSOR_DELAY_GAME)

    }

    fun stopMotionSensors() {
        motionSensorsEnabled = false
        sensorManager.unregisterListener(this)
    }

    private fun clearBuffers() {
        gyroscopeBuffer = CircularArray()
        magneticFieldBuffer = CircularArray()
        linearAccelerationBuffer = CircularArray()
        gravityBuffer = CircularArray()
    }

}