package at.fhooe.mc.linusheimboeck.recordingapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.FileProvider
import at.fhooe.mc.linusheimboeck.recordingapp.R
import at.fhooe.mc.linusheimboeck.recordingapp.utils.FileUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar.title = ""
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_appbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.app_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.info -> {
                // Toast.makeText(this, "Info clicked", Toast.LENGTH_SHORT).show()

                val intent = Intent(this, InformationActivity::class.java)
                val bundle = Bundle()
                bundle.putString("content", "info")
                intent.putExtras(bundle)
                startActivity(intent)
            }
            R.id.send -> {
                Toast.makeText(this, "Generating ZIP. Please wait.", Toast.LENGTH_SHORT).show()
                val future = doAsync {

                    val test = FileUtils()
                    val zipPath = test.zipSignatures()

                    uiThread {
                        val fileUri = FileProvider.getUriForFile(
                            applicationContext, applicationContext
                                .packageName.toString() + ".provider", File(zipPath)
                        )

                        val sendIntent = Intent()
                        sendIntent.action = Intent.ACTION_SEND
                        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                        sendIntent.type = "application/zip"
                        startActivity(sendIntent)
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
