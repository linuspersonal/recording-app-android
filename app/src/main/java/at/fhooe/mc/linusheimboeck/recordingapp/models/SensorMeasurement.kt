package at.fhooe.mc.linusheimboeck.recordingapp.models

import java.lang.StringBuilder

class SensorMeasurement(
    /**
     * In SystemClock.elapsedRealTimeNanos()
     */
    var timestamp: Long,
    var values: List<Float>
) {

    override fun toString(): String {
        val builder = StringBuilder()
        var count = 0
        values.forEach {
            builder.append("V$count: $it, ")
            count++
        }
        builder.append("Time: $timestamp")
        return builder.toString()
    }

}