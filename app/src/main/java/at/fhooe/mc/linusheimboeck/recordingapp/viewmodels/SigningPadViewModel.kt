package at.fhooe.mc.linusheimboeck.recordingapp.viewmodels

import android.content.SharedPreferences
import android.os.SystemClock
import android.util.Log
import androidx.collection.CircularArray
import androidx.core.hardware.display.DisplayManagerCompat
import androidx.lifecycle.ViewModel
import androidx.preference.PreferenceManager
import at.fhooe.mc.linusheimboeck.recordingapp.models.*
import at.fhooe.mc.linusheimboeck.recordingapp.utils.FileUtils
import java.io.File


class SigningPadViewModel : ViewModel() {

    private val TAG = "SigningPadViewModel"
    private val RELATIVE_PATH = "/Signatures"

    fun saveSignature(rawSignature: RawSignature, firstName: String?, lastName: String?, metadata: SignatureMetadata): ArrayList<SignatureDataPoint>? {

        // TODO: Async
        var signatureData = interpolate(rawSignature) ?: return null

        var first = "Unknown"
        if (firstName != null) {
            first = firstName
        }

        var last = "Unknown"
        if (lastName != null) {
            last = lastName
        }

        val fileUtils = FileUtils()
        val signature = Signature(first, last, System.currentTimeMillis(), rawSignature.genuine, File(RELATIVE_PATH))
        fileUtils.writeSignature(signatureData, signature, metadata)

        return signatureData
    }

    // 50 Hz is the maximum sensor recording speed
    private fun interpolate(rawSignature: RawSignature, sensingInterval: Int = 20): ArrayList<SignatureDataPoint>? {

        val signaturePoints = rawSignature.signaturePoints
        val startTime = signaturePoints[0].eventTime
        val endTime = signaturePoints[signaturePoints.size-1].eventTime
        val durationMillis = endTime - startTime

        val gyroReadings = preprocessSensorMeasurements(rawSignature.gyroscopeReadings, rawSignature.timeSystemDifference, startTime, endTime)
        val accelerationReadings = preprocessSensorMeasurements(rawSignature.linearAccelerationReadings, rawSignature.timeSystemDifference, startTime, endTime)
        val magnetometerReadings = preprocessSensorMeasurements(rawSignature.magneticFieldReadings, rawSignature.timeSystemDifference, startTime, endTime)
        val gravityReadings = preprocessSensorMeasurements(rawSignature.gravityReadings, rawSignature.timeSystemDifference, startTime, endTime)

        if (gyroReadings == null || accelerationReadings == null || magnetometerReadings == null || gravityReadings == null) {
            // Invalid sensor measurements
            return null
        }

        val interpolatedSignaturePoints = interpolateSignaturePoints(signaturePoints)
        val interpolatedGyroReadings = cutSensorMeasurements(interpolateSensorMeasurements(gyroReadings), startTime, endTime)
        val interpolatedAccelerationReadings = cutSensorMeasurements(interpolateSensorMeasurements(accelerationReadings), startTime, endTime)
        val interpolatedMagnetometerReadings = cutSensorMeasurements(interpolateSensorMeasurements(magnetometerReadings), startTime, endTime)
        val interpolatedGravityReadings = cutSensorMeasurements(interpolateSensorMeasurements(gravityReadings), startTime, endTime)

        val result = ArrayList<SignatureDataPoint>()

        var pos = 0
        while (pos <= durationMillis) {
            val poi = interpolatedSignaturePoints[pos]
            val gyr = interpolatedGyroReadings[pos]
            val acc = interpolatedAccelerationReadings[pos]
            val mag = interpolatedMagnetometerReadings[pos]
            val gra = interpolatedGravityReadings[pos]
            result.add(SignatureDataPoint(
                poi.x, poi.y, poi.vX, poi.vY, poi.strokeNumber, poi.touchSize,
                gyr.values[0], gyr.values[1], gyr.values[2],
                mag.values[0], mag.values[1], mag.values[2],
                acc.values[0], acc.values[1], acc.values[2],
                gra.values[0], gra.values[1], gra.values[2],
                poi.eventTime-startTime
            ))
            pos += sensingInterval
        }

        // Fix last point problem
        val lastResultPoint = result.last()
        if (lastResultPoint.strokeNumber == null) {
            Log.i(TAG, "FIXING LAST POINT IN INTERPOLATION")

            val poi = interpolatedSignaturePoints[interpolatedSignaturePoints.lastIndex]
            val gyr = interpolatedGyroReadings[interpolatedGyroReadings.lastIndex]
            val acc = interpolatedAccelerationReadings[interpolatedAccelerationReadings.lastIndex]
            val mag = interpolatedMagnetometerReadings[interpolatedMagnetometerReadings.lastIndex]
            val gra = interpolatedGravityReadings[interpolatedGravityReadings.lastIndex]

            if ((durationMillis - lastResultPoint.timestamp) < sensingInterval/2) {
                // Replace the faulty last line
                result[result.lastIndex] = SignatureDataPoint(
                    poi.x, poi.y, poi.vX, poi.vY, poi.strokeNumber, poi.touchSize,
                    gyr.values[0], gyr.values[1], gyr.values[2],
                    mag.values[0], mag.values[1], mag.values[2],
                    acc.values[0], acc.values[1], acc.values[2],
                    gra.values[0], gra.values[1], gra.values[2],
                    lastResultPoint.timestamp)
            } else {
                // Add the correct line as next line
                result.add(SignatureDataPoint(
                    poi.x, poi.y, poi.vX, poi.vY, poi.strokeNumber, poi.touchSize,
                    gyr.values[0], gyr.values[1], gyr.values[2],
                    mag.values[0], mag.values[1], mag.values[2],
                    acc.values[0], acc.values[1], acc.values[2],
                    gra.values[0], gra.values[1], gra.values[2],
                    lastResultPoint.timestamp + sensingInterval
                ))
            }
        }

        // Fix stroke number consistency problem
        var strokeNumber = -1
        var newStroke = true
        result.forEach {
            if (it.strokeNumber == null) {
                newStroke = true
            }
            else if (it.strokeNumber != null && newStroke) {
                newStroke = false
                strokeNumber++
            }

            if (it.strokeNumber != null) {
                it.strokeNumber = strokeNumber
            }
        }

        return result
    }

    private fun cutSensorMeasurements(sensorMeasurements: ArrayList<SensorMeasurement>, startTime: Long, endTime: Long): ArrayList<SensorMeasurement> {

        val iterator = sensorMeasurements.iterator()
        while (iterator.hasNext()) {
            val current = iterator.next()
            if (current.timestamp < startTime || current.timestamp > endTime) {
                iterator.remove()
            }
        }

        return sensorMeasurements
    }

    private fun interpolateSensorMeasurements(sensorMeasurements: ArrayList<SensorMeasurement>): ArrayList<SensorMeasurement> {

        val measurementsInterpolated = ArrayList<SensorMeasurement>()

        for (i in 0 until sensorMeasurements.size-1) {
            val current = sensorMeasurements[i]
            val next = sensorMeasurements[i+1]
            val amount = next.timestamp - current.timestamp

            if (amount > 1) {
                // Interpolate
                val interpolations = ArrayList<ArrayList<Float>>(current.values.size)
                for (j in current.values.indices) {
                    interpolations.add(j, linearInterpolation(current.values[j], next.values[j], amount.toInt()))
                }

                for (j in 0 until amount.toInt()) {
                    val measurement = ArrayList<Float>(interpolations.size)
                    for (k in 0 until interpolations.size) {
                        measurement.add(k, interpolations[k][j])
                    }
                    measurementsInterpolated.add(SensorMeasurement(current.timestamp+j, measurement.toList()))
                }
            } else if (amount.toInt() == 1) {
                // Nothing to interpolate
                measurementsInterpolated.add(current)
            }

            if (i >= sensorMeasurements.size-2) {
                // Finished, add last point
                measurementsInterpolated.add(next)
            }
        }

        return measurementsInterpolated
    }

    private fun interpolateSignaturePoints(signaturePoints: ArrayList<SignaturePoint>): ArrayList<SignaturePoint> {

        val signaturePointsInterpolated = ArrayList<SignaturePoint>()

        for (i in 0 until signaturePoints.size-1) {
            val current = signaturePoints[i]
            val next = signaturePoints[i+1]
            val amount = next.eventTime - current.eventTime

            if (amount > 1 && current.strokeNumber == next.strokeNumber) {
                // Interpolate
                val x = linearInterpolation(current.x, next.x, amount.toInt())
                val y = linearInterpolation(current.y, next.y, amount.toInt())
                val vX = linearInterpolation(current.vX, next.vX, amount.toInt())
                val vY = linearInterpolation(current.vY, next.vY, amount.toInt())
                val touchSize = linearInterpolation(current.touchSize, next.touchSize, amount.toInt())

                for (j in 0 until amount.toInt()) {
                    signaturePointsInterpolated.add(SignaturePoint(x[j], y[j], vX[j], vY[j], current.strokeNumber, touchSize[j], current.eventTime+j))
                }
            } else if (amount > 1 && current.strokeNumber != next.strokeNumber) {
                // The next value starts a new stroke, this is the last value of the current stroke
                signaturePointsInterpolated.add(current)
                for (j in 1 until amount.toInt()) {
                    signaturePointsInterpolated.add(SignaturePoint(null, null, null, null, null, null, current.eventTime+j))
                }
            } else if (amount == 1L) {
                // The next value is already in the next ms, so nothing to interpolate
                signaturePointsInterpolated.add(current)
            }

            if (i >= signaturePoints.size-2) {
                // Finished, add last point
                signaturePointsInterpolated.add(next)
            }
        }

        return signaturePointsInterpolated
    }

    /**
     * Returns the interpolation, including the start value, excluding the end value. Amount specifies the number of values to return, including start
     */
    private fun linearInterpolation(start: Float?, end: Float?, amount: Int): ArrayList<Float> {

        if (amount < 2 || start == null || end == null) {
            throw IllegalArgumentException()
        }
        val result = ArrayList<Float>()
        for (i in 0 until amount) {
            val currentValue = start + i * (end - start) / amount
            result.add(currentValue)
        }
        return result
    }

    private fun preprocessSensorMeasurements(measurements: CircularArray<SensorMeasurement>, timeSystemDifference: Long, startTime: Long, endTime: Long): ArrayList<SensorMeasurement>? {

        val result = ArrayList<SensorMeasurement>()

        var startValid = false
        var endValid = false
        for (i in 0 until measurements.size()-1) {

            val current = measurements[i]
            val next = measurements[i+1]
            current.timestamp = toUptimeMillis(current.timestamp, timeSystemDifference)

            if (current.timestamp <= startTime && toUptimeMillis(next.timestamp, timeSystemDifference) > startTime) {
                // This is the first relevant measurement
                startValid = true
            } else if (current.timestamp < endTime && toUptimeMillis(next.timestamp, timeSystemDifference) >= endTime) {
                // The next one is the last one that is relevant, add both and quit
                endValid = true
                next.timestamp = toUptimeMillis(next.timestamp, timeSystemDifference)
                result.add(current)
                result.add(next)
                break
            }

            if (startValid && !endValid) {
                // We want to add this measurement
                result.add(current)
            }
        }

        return if (startValid && endValid) {
            result
        } else {
            null
        }
    }

    /**
     * Converts a time given in elapsedRealTimeNanos reference system to the uptimeMillis reference system.
     * The timeSystemDifferenceMillis is the offset between the two referenceSystems in milliseconds
     */
    private fun toUptimeMillis(elapsedRealTimeNanos: Long, timeSystemDifferenceMillis: Long) : Long {

        return (elapsedRealTimeNanos / 1000000L) - timeSystemDifferenceMillis
    }

    private fun printSensorMeasurements(measurements: CircularArray<SensorMeasurement>) {

        for (i in 0 until measurements.size()) {
            Log.i(TAG, measurements[i].toString() + "\n")
        }
    }

    private fun printTimeDebugInformation(rawSignature: RawSignature) {

        Log.i(TAG, "Poi: " + rawSignature.signaturePoints[0].eventTime)
        Log.i(TAG, "Gyr: " + ((rawSignature.gyroscopeReadings[0].timestamp / 1000000L)-rawSignature.timeSystemDifference))
        Log.i(TAG, "Acc: " + ((rawSignature.linearAccelerationReadings[0].timestamp / 1000000L)-rawSignature.timeSystemDifference))
        Log.i(TAG, "Mag: " + ((rawSignature.magneticFieldReadings[0].timestamp / 1000000L)-rawSignature.timeSystemDifference))

        Log.i(TAG, "-----------------")

        Log.i(TAG, "Poi: " + rawSignature.signaturePoints[rawSignature.signaturePoints.size-1].eventTime)
        Log.i(TAG, "Gyr: " + ((rawSignature.gyroscopeReadings[rawSignature.gyroscopeReadings.size()-1].timestamp / 1000000L)-rawSignature.timeSystemDifference))
        Log.i(TAG, "Acc: " + ((rawSignature.linearAccelerationReadings[rawSignature.linearAccelerationReadings.size()-1].timestamp / 1000000L)-rawSignature.timeSystemDifference))
        Log.i(TAG, "Mag: " + ((rawSignature.magneticFieldReadings[rawSignature.magneticFieldReadings.size()-1].timestamp / 1000000L)-rawSignature.timeSystemDifference))

        Log.i(TAG, "-----------------")

        Log.i(TAG, "ERT: " + SystemClock.elapsedRealtime())
        Log.i(TAG, "UTM: " + SystemClock.uptimeMillis())
        Log.i(TAG, "Difference: " + rawSignature.timeSystemDifference)
    }
}
