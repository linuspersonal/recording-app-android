package at.fhooe.mc.linusheimboeck.recordingapp.models

import androidx.collection.CircularArray

class RawSignature(
    var signaturePoints: ArrayList<SignaturePoint>,
    var linearAccelerationReadings: CircularArray<SensorMeasurement>,
    var magneticFieldReadings: CircularArray<SensorMeasurement>,
    var gyroscopeReadings: CircularArray<SensorMeasurement>,
    var gravityReadings: CircularArray<SensorMeasurement>,
    /**
     * The time difference between SystemClock.elapsedRealTime() and SystemClock.uptimeMillis() at the time of signature commit.
     * Used to convert the different timestamps to the same reference system
     */
    var timeSystemDifference: Long,
    var genuine: Boolean
) {
}