package at.fhooe.mc.linusheimboeck.recordingapp.viewmodels

import androidx.lifecycle.ViewModel
import at.fhooe.mc.linusheimboeck.recordingapp.models.Signature
import at.fhooe.mc.linusheimboeck.recordingapp.models.SignatureDataPoint
import at.fhooe.mc.linusheimboeck.recordingapp.utils.FileUtils

class SignatureListViewModel : ViewModel() {

    private val RELATIVE_PATH = "/Signatures"

    fun getSignatureList(genuineOnly: Boolean = false) : ArrayList<Signature> {

        val fileUtils = FileUtils()
        val signatures = fileUtils.getSignatureList(RELATIVE_PATH)

        if (genuineOnly) {
            val iterator = signatures.iterator()
            while (iterator.hasNext()) {
                val current = iterator.next()
                if (!current.genuine) {
                    iterator.remove()
                }
            }
        }
        return signatures
    }

    fun getSignatureData(signature: Signature): String? {

        val fileUtils = FileUtils()
        return fileUtils.readSignatureString(signature.signatureFile)
    }
}
