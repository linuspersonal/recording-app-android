This repository contains the code for the Android recording app. It can be built using Android Studio, and the code is written with easy adaptability in mind.

If you just want to use a ready-to-use version of the app for your own research, the latest version is also available for download on the [Google Play Store](https://play.google.com/store/apps/details?id=at.fhooe.mc.linusheimboeck.recordingapp).
